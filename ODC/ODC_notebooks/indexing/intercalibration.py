import yaml
import rasterio
import uuid
from utils import *

def get_source(path):
    """
    copy .yaml metadata from source data and return it.
    """
    #Lire yaml
    with open(path) as file:
        # The FullLoader parameter handles the conversion from YAML
        # scalar values to Python the dictionary format
        doc = yaml.load(file, Loader=yaml.FullLoader)
    return doc

def prepare_yaml(doc, dataset, intercal, product):
    # Modifications du uuid, lineage, extension, 
    # crs (storage et geometry pareil que dans Sougeal), ajout
    # properties : intercalibration = radio and geo, modification du
    #  product_family en Sougeal.
    crs, geom_coord = get_projection(dataset)
    measurements = {}
    grids = {}

    bands = glob.glob(os.path.join(dataset, '*.tif'))
    for b in bands:
        name = b.split('/')[-1]
        name = name.split('_')[-1][:-4]
        measurements, grids = note_measurement(measurements, grids, "{0}".format(name.lower()), b)

    label = doc['label']

    doc['measurements'] = measurements
    doc['grids'] = grids
    doc['geometry']['coordinates'] = [geom_coord]
    doc['crs'] = str(crs)
    doc['lineage'].append(str(doc['id']))
    doc['id'] = str(uuid.uuid4())
    doc['properties']['intercalibration'] = str(intercal)
    doc['product_family'] = str(product)
    return doc, label

def main(path_source, dataset, intercal, product):
    """
    Main function copying metadata from source and create new yaml 
    metadata for datacube indexing
    """
    doc_source = get_source(path_source)

    doc, label = prepare_yaml(doc_source, dataset, intercal, product)

    #write yaml
    yaml_path = os.path.join(dataset, label + '_intercal.odc-metadata.yaml')
    with open(yaml_path, 'w') as stream:
        yaml.dump(doc, stream)

    return doc, yaml_path

if __name__ == "__main__":
    main()