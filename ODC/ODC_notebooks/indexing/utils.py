import os
import glob
import rasterio
from pyephem_sunpath.sunpath import sunpos
from statistics import mean
from pyproj import Transformer
import pytz
from timezonefinder import TimezoneFinder

def get_projection(dataset):
    # extract crs and polygon geometries
    path = glob.glob(os.path.join(dataset,'*.tif'))[0]
    with rasterio.open(str(path)) as img:
        left, bottom, right, top = img.bounds
        geom_coord =[[left,top], [right,top], [right,bottom], [left,bottom], [left,top]]
        crs = img.crs['init']
    return crs, geom_coord

def get_centroid(crs, geom):
    #reprojeter loc en wgs84
    loc = [mean(k) for k in zip(geom[0], geom[2])]
    transformer = Transformer.from_crs(crs, "EPSG:4326", always_xy=True)
    lon, lat = transformer.transform(*loc)
    centroid = (lon, lat)
    return centroid

def get_solar(centroid, dt):
    #reprojeter loc en wgs84
    lon, lat = centroid
    tz = 0
    alt, azm = sunpos(dt, lat, lon, tz, dst=False)
    return azm, alt

def get_grid(path):
    with rasterio.open(str(path)) as img:
        transform = list(img.transform)
        shape = list(img.shape)
    return shape, transform

def note_measurement(measurements, grids, name, path):
    measurements[name] = {
        'path': path
    }
    #grid
    shape, trans = get_grid(path)
    p_grid = {
            'shape': shape,
            'transform': trans,
        }
    if not grids:
        grid = 'default'
        grids[grid] = p_grid

    if p_grid != grids['default']:

        if p_grid not in grids.values():
            grid = str(len(grids.items()))
            grids[grid] = p_grid

        for g in grids:
            if p_grid == grids[g]:
                measurements[name]['grid'] = g
                break

    return measurements, grids