"""
This will convert Sentinel-2 data from Theia into
GeoTIFFS (COGs) with datacube metadata using the DEA naming conventions
for files.

    Prepare USGS Landsat Collection 1 data for ingestion into the Data Cube.
    This prepare script supports only for MTL.txt metadata file
    To Set the Path for referring the datasets -
    Download the  Landsat scene data from Earth Explorer or GloVis into
    'some_space_available_folder' and unpack the file.
    For example: yourscript.py --output [Yaml- which writes datasets into this file for indexing]
    [Path for dataset as : /home/some_space_available_folder/]
"""

from pathlib import Path
from typing import Union
import rasterio
from datetime import datetime, timezone
from eodatasets3 import DatasetAssembler
import os
import glob
import xml.etree.ElementTree as ET
from statistics import mean


def get_time(s2_path):
    tile_id = os.path.split(s2_path)[1]
    dt_text = tile_id.split("_")[-5]
    date = dt_text.split("-")[0]
    time = dt_text.split("-")[1]
    return datetime(int(date[:4]), int(date[4:6]), int(date[-2:]),
        int(time[:2]), int(time[2:4]), int(time[-2:]))

def get_xml(xml_path):

    tree = ET.parse(xml_path)
    root = tree.getroot()

    for angle in root.iter('Sun_Angles'):
        sun_zenith = float(angle.find("ZENITH_ANGLE").text)

    for angle in root.iter('Sun_Angles'):
        sun_azimuth = float(angle.find("AZIMUTH_ANGLE").text)

    view_zenith = []
    for angle in root.iter('Mean_Viewing_Incidence_Angle'):
        view_zenith.append(float(angle.find("ZENITH_ANGLE").text))
    view_zenith = mean(view_zenith)

    view_azimuth = []
    for angle in root.iter('Mean_Viewing_Incidence_Angle'):
        view_azimuth.append(float(angle.find("AZIMUTH_ANGLE").text))
    view_azimuth = mean(view_azimuth)

    return sun_zenith, sun_azimuth, view_zenith, view_azimuth

in_path = r'/mnt/data/imagery/sentinel-2/SENTINEL2A_20180517-111020-570_L2A_T30UXU_D_V1-7'
collection = Path(r'/home/alvarez_e/ODC/data')
xml_path = glob.glob(os.path.join(in_path,"*MTD_ALL.xml"))[0]

with DatasetAssembler(metadata_path=collection, allow_absolute_paths=True) as p:

    p.product_family = "satellite"
    p.instrument = "msi"
    p.platform = "Sentinel_2"
    p.dataset_version = "1.0.0"
    p.region_code = "T30UXU"
    p.properties["odc:file_format"] = "GeoTIFF"

    sun_zenith, sun_azimuth, view_zenith, view_azimuth = get_xml(xml_path)
    p.properties["eo:sun_azimuth"] = sun_azimuth
    p.properties["eo:sun_elevation"] = 90 - sun_zenith
    p.properties["eo:azimuth"] = view_azimuth
    p.properties["eo:off_nadir"] = view_zenith
    p.add_accessory_file("metadata", xml_path)

    # Date of acquisition (UTC if no timezone).
    p.datetime = get_time(in_path)
    # When the data was processed/created.
    p.processed_now() # Right now!
    # (If not newly created, set the date on the field: `p.processed = ...`)

    # Write our measurement from the given path, calling it 'blue'.
    #for files in collection:
    bands = ['B2','B3','B4','B5','B6','B7','B8','B8A','B11','B12']
    for band in bands:
        b_paths = glob.glob(os.path.join(in_path,'*{0}.tif'.format(band)))
        for b_path in b_paths:
            if 'SRE' in b_path:
                p.note_measurement("{0}_sre".format(band.lower()), Path(b_path))
            else:
                p.note_measurement(band.lower(), Path(b_path))

    masks =['IAB_R1', 'IAB_R2', 'SAT_R1',
        'SAT_R2', 'DFP_R1', 'DFP_R2', 'CLM_R1', 'CLM_R2', 
        'MG2_R1', 'MG2_R2', 'EDG_R1', 'EDG_R2']
    for mask in masks:
        m_path = glob.glob(os.path.join(in_path,'MASKS/*{0}.tif'.format(mask)))[0]
        p.note_measurement(mask.lower(), Path(m_path))

    # Complete the dataset.
    p.done()