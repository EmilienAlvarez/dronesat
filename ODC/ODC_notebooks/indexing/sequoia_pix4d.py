import yaml
from datetime import datetime, timezone
import os
import glob
import uuid
import click
import pytz
from timezonefinder import TimezoneFinder
from utils import *

def get_datetime(centroid, dataset):
    date_text = dataset.split('/')[-3]
    d = date_text.split("-")
    d = [int(i) for i in d]
    time_text = dataset.split('/')[-1]
    t = time_text.split("-")
    t = [int(i) for i in t]
    dt = d + t
    dt = datetime(*dt)
    #Applying time zone
    lon, lat = centroid
    tzf = TimezoneFinder().timezone_at(lng=lon, lat=lat)
    tz = pytz.timezone(tzf)
    dt = tz.localize(dt)
    dt = dt - dt.utcoffset()
    dt = dt.replace(tzinfo=pytz.utc)
    return dt

def prepare_yaml(dataset):

    crs, geom_coord = get_projection(dataset)
    centroid = get_centroid(crs, geom_coord)
    datetime = get_datetime(centroid, dataset)
    sun_azimuth, sun_elevation = get_solar(centroid, datetime)

    insrument = 'sequoia'
    platform = 'ebee_plus'
    version = '1.0.0'
    format = 'GeoTIFF'
    processing_time  = datetime.now()
    product = 'uav'
    lineage = []
    level = 'l2a'
    preprocess = 'pix4d'

    prod_name = platform + '_' + level + '_' + preprocess
    label = prod_name + '_' + str(datetime.date())

    measurements = {}
    grids = {}

    bands = ['green','red','rededge','nir']
    for b in bands:
        b_paths = glob.glob(os.path.join(dataset,'*{0}.tif'.format(b)))
        for b_path in b_paths:
            measurements, grids = note_measurement(measurements, grids, "{0}".format(b.lower()), b_path)


    # generating the .yaml document
    doc = {
        '$schema': 'https://schemas.opendatacube.org/dataset',
        'id': str(uuid.uuid4()),
        'label': str(label),
        'product': {
            'name': str(prod_name)
        },
        'crs': str(crs),
        'geometry': {
            'type': 'Polygon',
            'coordinates': [geom_coord],
        },
        'grids': grids,
        'properties':{
            'datetime': datetime,
            'eo:instrument': str(insrument),
            'eo:platform': str(platform),
            'odc:dataset_version': str(version),
            'odc:file_format': str(format),
            'odc:processing_datetime': processing_time,
            'odc:product_family': str(product),
            'eo:azimuth': float(0),
            'eo:off_nadir': float(0),
            'eo:sun_azimuth': float(sun_azimuth),
            'eo:sun_elevation': float(sun_elevation),
            'level': str(level),
            'preprocessing': str(preprocess)
        },
        'measurements': measurements,
        'lineage': lineage
    }
    return doc, label

@click.command(help="Prepare sequoia_pix4d dataset for indexation into the Data Cube.")
@click.argument('datasets', type=click.Path(exists=True, readable=True, writable=True), nargs=-1)
@click.argument('output', type=click.Path(exists=True, readable=True, writable=True), nargs=1)
def main(datasets, output):

    for d in datasets:
        #prepare
        doc, label = prepare_yaml(d)

        #write yaml
        yaml_path = os.path.join(output, label + '.odc-metadata.yaml')
        with open(yaml_path, 'w') as stream:
            yaml.dump(doc, stream)

if __name__ == "__main__":
    main()