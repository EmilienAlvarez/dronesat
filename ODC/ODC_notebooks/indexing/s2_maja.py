import yaml
from datetime import datetime, timezone
import os
import glob
import xml.etree.ElementTree as ET
import uuid
import click
import pytz
from statistics import mean
from utils import *

def get_datetime(dataset):
    tile_id = os.path.split(dataset)[1]
    dt_text = tile_id.split("_")[-5]
    date = dt_text.split("-")[0]
    time = dt_text.split("-")[1]
    dt = datetime(int(date[:4]), int(date[4:6]), int(date[-2:]),
        int(time[:2]), int(time[2:4]), int(time[-2:]))
    dt.replace(tzinfo=pytz.utc)
    return dt

def get_view(xml_path):

    tree = ET.parse(xml_path)
    root = tree.getroot()

    view_zenith = []
    for angle in root.iter('Mean_Viewing_Incidence_Angle'):
        view_zenith.append(float(angle.find("ZENITH_ANGLE").text))
    view_zenith = mean(view_zenith)

    view_azimuth = []
    for angle in root.iter('Mean_Viewing_Incidence_Angle'):
        view_azimuth.append(float(angle.find("AZIMUTH_ANGLE").text))
    view_azimuth = mean(view_azimuth)
    
    return view_zenith, view_azimuth

def get_region(dataset):
    tile_id = os.path.split(dataset)[1]
    region = tile_id.split("_")[3]
    return region

def prepare_yaml(dataset):
    
    xml_path = glob.glob(os.path.join(dataset,"*MTD_ALL.xml"))[0]

    crs, geom_coord = get_projection(dataset)
    centroid = get_centroid(crs, geom_coord)
    datetime = get_datetime(dataset)
    view_zenith, view_azimuth = get_view(xml_path)
    sun_azimuth, sun_elevation = get_solar(centroid, datetime)

    insrument = 'msi'
    platform = 'sentinel-2'
    version = '1.0.0'
    format = 'GeoTIFF'
    processing_time  = datetime.now()
    product = 'satellite'
    region = get_region(dataset)
    lineage = []
    preprocess = 'maja'
    level = 'l2a'

    prod_name = platform + '_' + level + '_' + preprocess
    label = prod_name + '_' + region + '_' + str(datetime.date())

    measurements = {}
    grids = {}

    bands = ['B2','B3','B4','B5','B6','B7','B8','B8A','B11','B12']
    for b in bands:
        b_paths = glob.glob(os.path.join(dataset,'*{0}.tif'.format(b)))
        for b_path in b_paths:
            if 'SRE' in b_path:
                measurements, grids = note_measurement(measurements, grids, "{0}_sre".format(b.lower()), b_path)
            else:
                measurements, grids = note_measurement(measurements, grids, b.lower(), b_path)

    masks =['SAT_R1',
        'SAT_R2', 'DFP_R1', 'DFP_R2', 'CLM_R1', 'CLM_R2', 
        'MG2_R1', 'MG2_R2', 'EDG_R1', 'EDG_R2']
    for m in masks:
        m_path = glob.glob(os.path.join(dataset,'MASKS/*{0}.tif'.format(m)))[0]
        measurements, grids = note_measurement(measurements, grids, m.lower(), m_path)

    # generating the .yaml document
    doc = {
        '$schema': 'https://schemas.opendatacube.org/dataset',
        'id': str(uuid.uuid4()),
        'label': str(label),
        'product': {
            'name': str(prod_name)
        },
        'crs': str(crs),
        'geometry': {
            'type': 'Polygon',
            'coordinates': [geom_coord],
        },
        'grids': grids,
        'properties':{
            'datetime': datetime,
            'eo:azimuth': float(view_azimuth),
            'eo:instrument': str(insrument),
            'eo:off_nadir': float(view_zenith),
            'eo:platform': str(platform),
            'eo:sun_azimuth': float(sun_azimuth),
            'eo:sun_elevation': float(sun_elevation),
            'odc:dataset_version': str(version),
            'odc:file_format': str(format),
            'odc:processing_datetime': processing_time,
            'odc:product_family': str(product),
            'odc:region_code': str(region),
            'level': str(level),
            'preprocessing': str(preprocess)
        },
        'measurements': measurements,
        'accessories': {
            'metadata': {
                'path': str(xml_path)
            }
        },
        'lineage': lineage
    }
    return doc, label

@click.command(help="Prepare S2 MAJA dataset for indexation into the Data Cube.")
@click.argument('datasets', type=click.Path(exists=True, readable=True, writable=True), nargs=-1)
@click.argument('output', type=click.Path(exists=True, readable=True, writable=True), nargs=1)
def main(datasets, output):

    for d in datasets:
        #prepare
        doc, label = prepare_yaml(d)

        #write yaml
        yaml_path = os.path.join(output, label + '.odc-metadata.yaml')
        with open(yaml_path, 'w') as stream:
            yaml.dump(doc, stream)

if __name__ == "__main__":
    main()